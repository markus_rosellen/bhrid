# Bhrìd TeX — An experimental format for TeX

Bhrìd TeX started in 2001 as an experimental format whose general
design should follow milestones settled by plain TeX while adding some
higher level features common in other formats and missing in plain
TeX.

This experimental format aims at defining clean and extensible
programming paradigms.

Michael Grünewald in Bonn, on April 11, 2014
